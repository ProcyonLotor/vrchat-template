# VRChat Unity project template
Omni-template for making VRChat content

## Synopsis
The template have:
- Unity Editor settings for work with Git
- Git settings for work with Unity Editor
- Git LFS attributes
- Bootstrapper for automated SDK packages download
- Compatible with CreatorCompanion (but it may fuck up line endings!)

> If VPM Resolver is not present, Bootstapper will install latest version of it which might not be compatible with older Unity versions.
> Add `com.vrchat.core.vpm-resolver` to git, if you want to lock it (you need to adjust `.gitignore` file for this).

The repository have separate branches for separate content types, the only difference between them is default packages in `Packages/vpm-manifest.json`
> Branches structure
> ```
> -master-+---------------------->
>         |\
>         | \-world-------------->
>          \
>           \-avatar------------->
> ```

## How to use
1. `git init` a new repo from the branch you want
2. Init Git LFS with `git lfs install`
3. Open the project in Unity
4. Let VPM resolve all packages
5. Load an example and check that everything works without errors

> After opening the project in Unity Editor you might want to add **all** new files to Git.

> **IMPORTANT**: When opening a previously worked on project on a new machine, Unity may warn you about script errors and suggest you enter Safe Mode,
> this is because VPM has not loaded yet and didn't resolved the required packages, ignore the errors and continue loading.
> Sometimes you may need to reopen the project couple times for all packages to resolve and scripts to recompile.

## License
For project's license see [LICENSE](./LICENSE) file.
Project contains `com.vrchat.core.bootstrap` package, for license see [Packages/com.vrchat.core.bootstrap/License.md](./Packages/com.vrchat.core.bootstrap/License.md) file.
